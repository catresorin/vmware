function Get-CiVmWithIp ($Org,$IP) {
    foreach ($Civm in get-civm -Org $Org){
        Write-Progress -Activity "Checking $CiVm"
        foreach ($NetworkConnection in ($Civm.ExtensionData.section | Where-Object {$_.Type -eq 'application/vnd.vmware.vcloud.networkConnectionSection+xml'}).NetworkConnection){
            if ($NetworkConnection.IpAddress -eq $IP){
                write-host "$CiVm is using $IP"
            }
        }
    }
}

function Get-CiVmStorageProfile ($OrgVdc){
    $CiVmList = get-civm -OrgVdc $OrgVdc
    foreach ($Civm in $CiVmList){
        write-host $CiVm.Name $CiVm.ExtensionData.StorageProfile.Id
    }
}

function Get-VdcStorageProfile ($OrgVdc){
    $StorageProfiles = Get-OrgVdc -Name $OrgVdc
    foreach ($StorageProfile in $StorageProfiles.extensiondata.vdcstorageProfiles.vdcstorageprofile){
        write-host $OrgVdc $StorageProfile.Name $StorageProfile.Id
    }
}

function Get-WrongVmVdcStorageProfile ($OrgVdc) {
    $StorageProfilesArray = @()
    $VmsWithWrongProfiles = @()
    $StorageProfiles = Get-OrgVdc -Name $OrgVdc
    $CiVmLists = Get-CIVM -OrgVdc $OrgVdc
    $FileCsv = 'C:\wrongsp.csv'
    $StorageProfilesCsv = ""
    foreach ($StorageProfile in $StorageProfiles.extensiondata.vdcstorageProfiles.vdcstorageprofile){
        $StorageProfilesArray += New-Object psobject -Property @{ 
            SPID = $StorageProfile.Id
            SPName = $StorageProfile.Name
        }

        $StorageProfilesCsv += "$($StorageProfile.Name);$($StorageProfile.Id);"
        
    }
    
    foreach ($CiVm in $CiVmLists){
        if ($CiVm.ExtensionData.StorageProfile.Id -notin $StorageProfilesArray.SPID){
            $CsvContent = "$($CiVm.Name);$($OrgVdc);$($CiVm.ExtensionData.StorageProfile.Id);$($CiVm.ExtensionData.StorageProfile.Name);$($StorageProfilesCsv)"
            $CsvContent >> $FileCsv
            $VmsWithWrongProfiles  += New-Object psobject -Property @{
                VM = $CiVm.Name
                VDC = $OrgVdc
                VmSPId = $CiVm.ExtensionData.StorageProfile.Id 
                VmSPname = $CiVm.ExtensionData.StorageProfile.Name
                VdcSP = $StorageProfilesArray
            }
        }
      
    }

    $VmsWithWrongProfiles | Select-Object VDC,VM,VMSPId,VmSPname,VdcSp
}

function Get-HbaNr {
    foreach ($VmHost in get-vmhost){
        if ($VmHost.extensiondata.hardware.systeminfo.model | Where-Object {$_ -like 'PowerEdge*'}){
            $Hbas = ($Vmhost | Get-VMHostHba | where-object {$_.Model -like 'QLE2690*'}).count
            if ($Hbas -gt 2) {
                write-host "$VmHost HBAS: $Hbas Model: $($VmHost.extensiondata.hardware.systeminfo.model)"
            }
        }
    }
}

function Get-CiVmNicStatus ($OrgVdc){
    $NicReport = @()
    foreach ($CiVm in Get-CIVM -OrgVdc $OrgVdc){
        $NicConnected = @()
        $NicDisconnected = @()
        foreach ($Nic in ($CiVm.ExtensionData.section | Where-Object {$_.Type -eq 'application/vnd.vmware.vcloud.networkConnectionSection+xml'}).networkConnection){
            if ($Nic.IsConnected -eq $False){
                $NicDisconnected += New-Object psobject -Property @{
                    Index = $Nic.NetworkConnectionIndex
                    Network = $Nic.Network
                }
            } else {
                $NicConnected += New-Object psobject -Property @{
                    Index = $Nic.NetworkConnectionIndex
                    Network = $Nic.Network
                }
            }
        }

        
        $NicReport += New-Object psobject -Property @{
            VirtualMachine = $CiVm.Name
            Connected = $NicConnected
            Disconnected = $NicDisconnected
        }
    }

    $NicReport | Select-Object VirtualMachine,Connected,Disconnected 
}


function Get-VmdkToOSFs ($VirtualMachine){
    foreach ($hd in get-vm $VirtualMachine | Get-HardDisk){
        $HdSize = $hd.extensiondata.CapacityinKB / 1024 / 1024
        $Controller = $hd.Parent.Extensiondata.Config.Hardware.Device | where-object {$_.Key -eq $hd.ExtensionData.ControllerKey}
        write-host "$VirtualMachine $($hd.extensiondata.DeviceInfo.Label) [Size] $HdSize [UUID] $($hd.extensiondata.backing.uuid) [SCSiID ] $($Controller.BusNumber) : $($hd.ExtensionData.UnitNumber)"
    }
}

function Create-VappFromVms ($SourceOrgVdc,$DestOrgVdc,$Vapp){
    foreach ($vm in get-civm -orgvdc $SourceOrgVdc -VApp $Vapp){
        $NewVapp = $Vm.name + "_Vapp"
        New-CiVapp -OrgVdc $DestOrgVdc -name $NewVapp
    }
}

function Assign-DirectVappNetwork {
    param(
        [array] $OrgVdcNetworks,
        [string] $Civapp,
        [string] $OrgVdc
    )
    foreach ($OrgVdcNetwork in $OrgVdcNetworks){
        New-CIVAppNetwork -ParentOrgVdcNetwork (Get-OrgVdc -name $OrgVdc | Get-OrgVdcNetwork -name $OrgVdcNetwork ) -VApp (Get-OrgVdc -name $OrgVdc | Get-CiVapp $Civapp) -Direct
    }
}

function Get-PlacementOption {
    param(
        [string] $SourceDatastoreCluster,
        [string] $DestinationDatastoreCluster,
        [string] $DestinationVmCluster,
        [switch] $Move

    )

    $VmsInCluster = (Get-Cluster $DestinationVmCluster | get-vm).name
    
    $index = 0
    foreach ( $Vm in Get-DatastoreCluster -name $SourceDatastoreCluster | get-vm | Sort-Object -Descending -Property UsedSpaceGB ){
        if ($Vm -in $VmsInCluster){
            $index++
            $VmInfo = get-vm -name $vm
            if ($VmInfo | Get-CDDrive | Where-Object {$_.IsoPath -ne $null}){
                write-host "[$index] $VmCluster $($VmINfo.name) $($VmInfo.UsedSpaceGB) Unmounte ISO first to migrate the vm "
            } else {
                
                if ($Move) {
                    write-host "Moving $($VmInfo.name)"
                    Move-VM -vm $($VmInfo.name) -Datastore $DestinationDatastoreCluster
                } 
                else {     
                    write-host "[$index] $VmCluster $($VmINfo.name) $($VmInfo.UsedSpaceGB) MountedISO: NO ISO Mounted MOVE: move-vm -vm $($VmInfo.name) -Datastore $($DestinationDatastoreCluster)"
                }
            }
            
        }
    }
}

function Get-CatalogTemplatesItems {
    $CatalogItems = @()
    foreach ($CiVappTemplate in Get-CIVAppTemplate){
        $OrgVdc = $CiVappTemplate.OrgVdc
        $Catalog = $CiVappTemplate.Catalog
        $VMS = @()
        foreach ($Children in (Get-CIView -id $CiVappTemplate.id).children.vm){
            $VMS += $Children.name
        }
        $Children = $VMS

        $CatalogItems += New-Object psobject -Property @{
            OrgVDC = $OrgVdc
            Catalog = $Catalog
            Template = $CiVappTemplate.Name
            VMS = $Children
        }
    }
    
    $CatalogItems | Select-Object OrgVdc,Catalog,Template,VMS
}


function get-macvlantoip { 
    param($cluster)
    $netinfo = @()
    foreach ($vm in get-cluster $cluster | get-vm) {
        foreach ($ip in (get-vm -name $vm.name).extensiondata.guest.net.ipaddress){
            $netinfo  += New-Object psobject -Property @{
                Vm = $vm.Name
                mac = ((get-vm -name $vm.name).extensiondata.guest.net | where {$_.ipaddress -contains $ip  }).macaddress
                portGroup = ((get-vm -name $vm.name).extensiondata.guest.net | where {$_.ipaddress -contains $ip}).network
                ip = $ip
                AssetId = (get-vm -name $vm.name | Get-Annotation -name AssetId).value
            }    
        }
    }

    $netinfo | select vm,mac,ip,portgroup 
    $netinfo | Export-Csv netinfo.csv
}

function set-civmnicmacaddress {
    param ($OrgVdc,$VMName,$MacAddress)
    #this function will set the mac address for the primary interface only
    #more logic needs to be added to do it for all the nics or for specific ones
    $vm = get-civm -OrgVdc $OrgVdc -name $VMName
    $vm.ExtensionData.Section[2].NetworkConnection[0].MACAddress = $MacAddress
    $vm.ExtensionData.Section[2].UpdateServerData()
    
}

function get-civmnicinfo {
    param ($OrgVdc)
    $i=0
    foreach ($vm in get-civm -OrgVdc $OrgVdc){
        $i++
        foreach ($nic in $vm.ExtensionData.Section[2].networkConnection){
            $nic | Select-Object @{Name="VM $($i)";Expression={$VM.name}},@{Name='NIC';Expression={$_.NetworkConnectionIndex}},IpAddress,MACAddress,Network
        }
    }
}

function Capacity-Edgema {
    $INDHosts = @()
    $GKLHosts = @()
    $INDVMs = 0
    $INDVcpus = 0
    $INDCores = 0
    $GKLVMs = 0
    $GKLvcpus = 0
    $GKLCores = 0
    $Constant = (Get-VMHost -Location edgema | Select-Object -First 1).extensiondata.hardware.cpuinfo.numcpucores
    foreach ($vmhost in  Get-VMHost -Location edgema | select @{"Name"="VMHOST";Expression={$_.name}},@{"Name"="Tag";Expression={($_ | Get-TagAssignment -Category datacenter).tag}}){
        if ($vmhost.tag.name -eq 'IND'){
            $INDVMs = $INDVMs + (get-vmhost -name $vmhost.vmhost | get-vm).count
            $INDCores = $INDCores + (get-vmhost -name $vmhost.vmhost).ExtensionData.hardware.cpuinfo.numcpucores
            foreach ($vm in get-vmhost -name $vmhost.vmhost | get-vm){
                $INDVcpus = $INDVcpus + $vm.ExtensionData.summary.config.numcpu
            }
        }

        if ($vmhost.tag.name -eq 'GKL'){
            $GKLVMs = $GKLVMs + (get-vmhost -name $vmhost.vmhost | get-vm).count
            $GKLCores = $GKLCores + (get-vmhost -name $vmhost.vmhost).ExtensionData.hardware.cpuinfo.numcpucores
            foreach ($vm in get-vmhost -name $vmhost.vmhost | get-vm){
                $GKLVcpus = $GKLVcpus + $vm.ExtensionData.summary.config.numcpu
            }
            
        }
    }

    write-host "IND HOSTS"
    write-host "CORES: $INDCores VMS: $($INDVMs) VCPUS: $($INDVcpus) RATIO: $($INDVcpus/($INDCOres-$Constant))"
    write-host "GKL HOSTS"
    write-host "CORES: $GKLCores VMS: $($GKLVMs) VCPUS: $($GKLVcpus) RATIO: $($GKLVcpus/($GKLCOres-$Constant))"
    write-host "Together"
    write-host "CORES: $($INDCores+$GKLCores) VMS: $($INDVMs+$GKLVMs) VCPUS: $($INDVcpus+$GKLvcpus) RATIO: $($INDVcpus/($INDCOres-$Constant) + $GKLVcpus/($GKLCOres-$Constant))"
    
   do {
       $INDCores = $INDCores + $Constant
       write-host $INDCores
   } while ($INDVcpus/($INDCOres-$Constant) -ge 2)

   write-host "INDCores: $INDCores"

    
}

function VMstatus-DuplicatedVm {
    $vmstatus = Invoke-RestMethod -uri "https://vmstatus.yacn.dk/api/Inventory/VM/ALL"
    $vmstatus.Data | Group-Object -Property Assetid | Where-Object {$_.Count -gt 1} | Where-Object {$_.Name -notlike '' } | Select-Object -ExpandProperty Group | Select-Object AssetID,Name,ClusterName,PowerState
}

function Get-EmptyVdc {
    foreach ($OrgVdc in Get-OrgVdc){
        $Civms = Get-OrgVdc -name $OrgVdc | get-civm
        if ($Civms.Count -eq 0){
            write-host "[VDC]: $($OrgVdc) [Status]: $($OrgVdc.extensiondata.IsEnabled) [VMS]: $($Civms.Count)"
        } 
    }
}

function Get-RoutedVdc {
    #connection to vcenter and cloud
    param (
        [Parameter(Mandatory=$false)][switch]$details,
        [Parameter(Mandatory=$false)][array]$VDCs
    )
    if ($VDCs){$VDC = get-orgvdc -name $VDCs} else {$VDC = get-orgvdc}
    foreach ($OrgVdc in $VDC){
        write-host "-------- $OrgVdc --------"
        foreach ($network in Get-OrgVdcNetwork -orgvdc $OrgVdc | Where-Object {$_.NetworkType -eq 'Routed'}){
            $VMs = (Get-CIVM -OrgVdcNetwork $network.name).count
            write-host "$($OrgVdc.extensiondata.providervdcreference.name) $OrgVdc $($network.name) $($network.ExtensionData.edgegateway.name) VMs Usingit: $VMs"
            if ($VMs -gt 0){
                if ($details -eq $true){
                    foreach ($CiVm in Get-CIVM -OrgVdcNetwork $network.name -OrgVdc $OrgVdc.name ){
                        $VCVmId = 'VirtualMachine-' + $CiVm.extensiondata.vcloudextension.any.vmvimobjectref.moref
                        $VCVm= get-vm -id $VCVmId
                        
                        if ($VCVm.extensiondata.customValue | Where-Object {$_.Key -eq '904'}){
                            $Rubrik = 'YES'
                        } else {
                            $Rubrik = 'NO'
                        }

                        $TotalVMStorageSize = 0
                        foreach ($HD in $Civm.extensiondata.getvirtualhardwaresection().Item | Where-Object { $_.Description -like “Hard Disk”}){
                            $TotalVMStorageSize += [System.Numerics.BigInteger]$HD.VirtualQuantity.value
                            $TotalStorageSize += $TotalVMStorageSize
                        }
                        write-host "Rubrik: $Rubrik VM: $($CiVm.name ) Tier: $($CiVm.ExtensionData.StorageProfile.name) StorageSizeGb: $($TotalVMStorageSize/1024/1024/1024)"
                    }
                }
            }   
        }

        $VDCDetails | select VDC,VM,Rubrik | fl
    }
}

function get-vCloudRubrikVms{
    #connect to vcloud and vcenter
    $RubrikVmsList = @()
    foreach ($Org in Get-Org){
        $CiVms = get-civm -org $org
        foreach ($CiVm in $CiVms ){
            $VcenterVMMoref = 'VirtualMachine-' + $CiVm.ExtensionData.vcloudextension.any.vmvimobjectref.moref
            if ((Get-Vm -id $VcenterVMMoref).extensiondata.customValue | Where-Object {$_.Key -eq '904'}){
                write-host " $($Org.name) $($CiVm.Name) $((Get-Vm -id $VcenterVMMoref).UsedSpaceGB) "
                $RP = get-resourcepool -id ('ResourcePool-' + (get-vm -id $VcenterVMMoref).ExtensionData.ResourcePool.value)
                $RubrikVmsList += New-Object psobject -Property @{
                    Org = $Org.name
                    CustomerId = ((Get-Vm -id $VcenterVMMoref).extensiondata.customValue | Where-Object {$_.Key -eq '1203'}).value
                    CloudVm = $CiVm.Name
                    ResourcePool = $RP.name
                    VcenterVm = (Get-Vm -id $VcenterVMMoref).name
                    SizeGb =  (Get-Vm -id $VcenterVMMoref).UsedSpaceGB
                }
            }
        }
    }
    return $RubrikVmsList
}


function clone-vappnetworkfirewallrules {
    #https://communities.vmware.com/t5/vCloud-Director-PowerCLI/Appending-Firewall-Rules-to-vShield-Edge-with-PowerCLI-Script/td-p/2667772
    #https://communities.vmware.com/t5/VMware-vCloud-API-Discussions/Add-Firewall-Rules-to-a-vCloud-Director-vApp-Network-via/td-p/434854
    param ($SourceVDC,$DestinationVDC,$SourceCiVapp,$DestinationCivapp,$SourceNetwork,$DestinationNetwork)

    #get the source and destination networks and the configuration sections
    $SourceNetworkConfigSection = (Get-CIVApp -name $SourceCiVapp -OrgVdc $SourceVDC).ExtensionData.GetNetworkConfigSection()
    $SourceVappNetwork = $SourceNetworkConfigSection.NetworkConfig | Where-Object {$_.networkName -eq $SourceNetwork}

    $DestinationVappNetworkConfigSection = (Get-CIVApp -name $DestinationCivapp -OrgVdc $DestinationVDC).extensiondata.GetNetworkConfigSection()
    $DestinationVappNetwork = $DestinationVappNetworkConfigSection.NetworkConfig | where-object {$_.networkName -eq $DestinationNetwork}
    #endd

    #define a new FW Service
    $NewFireWallService = New-Object vmware.vimautomation.cloud.views.firewallservice
    $NewFireWallService.DefaultAction = "drop"
    $NewFireWallService.LogDefaultAction = $false
    $NewFireWallService.IsEnabled = $True

    #define the FW Rules
    $NewFireWallService.FirewallRule = New-Object vmware.vimautomation.cloud.views.firewallrule
    #$NewFireWallService.FirewallRule = @()
    $i = 0
    #do{
    foreach ($FirewallRule in $SourceVappNetwork.configuration.features.FirewallRule){
        
        #$FirewallRule = $SourceVappNetwork.configuration.features.FirewallRule[$i]
        write-host "FW $($i+1)  $($FirewallRule.Description) ------------------"
        $FirewallRule.DestinationIp
        if ($FirewallRule.DestinationIp){
            $IsEnabled = [System.Convert]::ToBoolean($FirewallRule.IsEnabled)
            $Description = $FirewallRule.Description
            $Policy = $FirewallRule.Policy
            $DestinationPortRange = $FirewallRule.DestinationPortRange
            $SourcePortRange = $FirewallRule.SourcePortRange
            $SourceIp = $FirewallRule.SourceIp

            #start adding fw rules
            $NewFireWallService.FirewallRule += New-Object vmware.vimautomation.cloud.views.firewallrule
            $NewFireWallService.FirewallRule[$i].IsEnabled = $IsEnabled
            $NewFireWallService.FirewallRule[$i].Description = $Description
            $NewFireWallService.FirewallRule[$i].Policy = $Policy
            $NewFireWallService.FirewallRule[$i].DestinationPortRange = $DestinationPortRange
            $NewFireWallService.FirewallRule[$i].DestinationIp = $FirewallRule.DestinationIp
            $NewFireWallService.FirewallRule[$i].SourcePortRange = $SourcePortRange
            $NewFireWallService.FirewallRule[$i].SourceIp = $SourceIp
           
            $NewFireWallService.FirewallRule[$i].Protocols = New-Object vmware.vimautomation.cloud.views.firewallRuleTypeProtocols
            foreach ($protocol in $FirewallRule.protocols){
                if($protocol.any) {
                    $NewFireWallService.FirewallRule[$i].Protocols.Any = $True
                }
                if($protocol.icmp) {
                    $NewFireWallService.FirewallRule[$i].Protocols.Icmp = $True
                }
                if($protocol.tcp) {
                    $NewFireWallService.FirewallRule[$i].Protocols.Tcp = $True
                }
                if($protocol.udp) {
                    $NewFireWallService.FirewallRule[$i].Protocols.Udp = $True
                }
            }

            
            #$NewFireWallService.FirewallRule[$i]
            $i++
        } else {
            write-host "[$($SourceCiVapp) - $($SourceNetwork)]Check the rule # $($i+1) $($FirewallRule.Description) after the migration, this rule is pointing to a destination vm not IP" 
        }

        
    } #while ($i -le $SourceVappNetwork.configuration.features.FirewallRule.count)

    write-host "Total Rules $i"
    write-host 'NewFireWallService-------'
    #for some reason when one of the rules contains a vm set as DestinationVM i get a new extra rule which is empty which leads the script UpdateServerData() method to fail as no empty rules are allowed
    #add $NewFireWallService.FirewallRule | Select-Object -First $($i - 1) to remove the last line
    #otherwise the bellow line should be commented out
    $NewFireWallService.FirewallRule =  $NewFireWallService.FirewallRule | Select-Object -First $($i - 1)
    $NewFireWallService
    
    #$DestinationVappNetwork.Configuration.Features = $SourceVappNetwork.Configuration.Features | where-object {!($_ -is [VMware.VimAutomation.Cloud.Views.FirewallService])}
    $DestinationVappNetwork.Configuration.Features = $SourceVappNetwork.Configuration.Features | where-object {($_ -is [VMware.VimAutomation.Cloud.Views.DhcpService])}# -and $_ -is [VMware.VimAutomation.Cloud.Views.NatService])}
    $DestinationVappNetwork.Configuration.Features += $NewFireWallService

    #write-host ".Configuration.Features [1]]]]]"
    #$DestinationVappNetwork.Configuration.Features 
    #write-host ".Configuration.Features.FWRULES [1]]]]]"
    #$DestinationVappNetwork.Configuration.Features.firewallrule
    $DestinationVappNetworkConfigSection.UpdateServerData()
    #$DestinationVappNetwork.Configuration.Features.FirewallRule 
}
